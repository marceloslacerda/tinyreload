/* See LICENSE file for license details. */

function getResourceCallback(url, fun) {
    $.get(url).always(function (data) {
        if(data.status == 200) {
            fun(data.responseText);
        }
    });
}

function tinyReload() {
    old={};
    errors={}
    $("script").each(function() {
        var url = $(this).attr("src");
        getResourceCallback(url, function (data) { old[url] = data; });
        errors[url] = null;
    });

    var url = document.URL;

    getResourceCallback(url, function (data) { old[url] = data; });

    _.each(_.map(document.styleSheets, 'href'),  function (url) {
        getResourceCallback(url, function (data) { old[url] = data; });
    });

    window.setInterval(compare, 2000);
}

function compare() {
    _.map(old, function (v, k) {
        getResourceCallback(k, function (data) {
            if(data == v) {
//                console.log('Equal');
                if(errors[k] != null) {
                   console.log(k + ' was fixed.');
                   errors[k] = null;
                }
            }
            else {
                if(k.endsWith('.js')) {
                    try {
                        eval(data);
                    } catch (ex) {
                        err = {
                          line : ex.lineNumber,
                          column : ex.columnNumber,
                          message : ex.message
                        };
                        if(errors[k] == null || (
                           err.message != errors[k].message ||
                           err.line != errors[k].line ||
                           err.column != errors[k].column)) {
                            errors[k] = err;
                            console.log('Error on ' + k);
                            console.log(ex);
                            console.log('Line number: ' + ex.lineNumber);
                            code = data.split('\n');
                            line = err.line - 1;
                            console.log(code[line - 1] +
                              '\n' + code[line] +
                              '\n' + code[line + 1]);
                        }
                        return;
                    }
                }
                window.location.reload();
            }
        });
    });
}

$(document).ready(function(){
    tinyReload();
});
