#Tinyreload

Auto reloads the contents of the page that the script is loaded in if the
html, css or script are changed in the disk.

##Dependencies

* lodash 3.0.0(or possibly underscorejs)
* jquery 2.1.3

##Usage

Copy this folder to the application folder and insert this line in the header of
the main page, preferably, right below the loading jquery and lodash.


```
#!html

<script src="tinyreload/reload.js"></script>
```


Now, whenever you change(and save) either the js, html, or css the page will
reload.

##Known limitations

* Doesn't account for directory changes.
* Image changes(possibly easy to implement).
* HTML errors may prevent reloads.

##Alternatives

* Using mozrepl integrated with the IDE(editor) of your choice.
* Using reactjs.
